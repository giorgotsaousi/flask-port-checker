
from socket import *

def port_check(ipAdress, port):
    target = ipAdress
    t_IP = gethostbyname(target)
    target_port = int(port)
    
    print ('Starting scan on host: ', t_IP)
    
        
    s = socket(AF_INET, SOCK_STREAM)
            
    conn = s.connect_ex((t_IP, target_port))
    if(conn == 0) :
        # print ('Port %d: OPEN' % (target_port,))
        return 'Port %d: OPEN' % (target_port,)
    else:
        return 'Port %d: CLOSED' % (target_port,)

    s.close()



def multi_port_check(ipAdress, port_start, port_end):
    target = ipAdress
    t_IP = gethostbyname(target)
    target_port_start = int(port_start) 
    target_port_end = int(port_end) + 1
    print ('Starting scan on host: ', t_IP)
    print (target_port_start)
    print (target_port_end)
    output_open = []
    output_closed = []        
    s = socket(AF_INET, SOCK_STREAM)

    for i in range(target_port_start, target_port_end):
        s = socket(AF_INET, SOCK_STREAM)
        conn = s.connect_ex((t_IP, i))
        print (i)
        if(conn == 0):
            output_open.append('Port %d: OPEN' % (i,))
        else:
            output_closed.append('Port %d: CLOSED' % (i,))

    return output_closed + output_open
    s.close()


# import subprocess

# IpAdress = "gtsaousi.com"
# port = "80"
# cmd = "telnet", IpAdress, port
# p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
# (output, err) = p.communicate()

# substring = "Connected"
# if substring in str(output):
#     print ("port is open")
     
# else:
#     print ("port is closed")
