from flask import Flask, request, render_template, flash
from python_script import port_check
from python_script import multi_port_check
from python_script_multithread import multi_thread_port_check

app = Flask(__name__)


@app.route('/')
def my_form():
    return render_template('my-form.html')

@app.route('/multi')
def multi_form():
    return render_template('multi-form.html')


@app.route('/thread')
def thread_form():
    return render_template('thread-form.html')


@app.route('/', methods=['POST'])
def my_form_post():
    ipAdress = request.form['text']
    port = request.form['port']
    port_check_result = port_check(ipAdress, port)
    #return 'IP/Domain: {} at {} '.format(ipAdress, port_check_result)
    return render_template('my-form.html', ipAdress=ipAdress, port_check_result=port_check_result)


@app.route('/multi', methods=['POST'])
def multi_form_post():
    ipAdress = request.form['text']
    port_start = request.form['port_first']
    port_end = request.form['port_last']
    port_check_result = multi_port_check(ipAdress, port_start, port_end)
    print (*port_check_result)
    #return 'IP/Domain: {} at {} '.format(ipAdress, port_check_result)
    return render_template('multi-form.html', ipAdress=ipAdress, port_check_result=port_check_result)

@app.route('/thread', methods=['POST'])
def thread_form_post():
    ipAdress = request.form['text']
    port_start = request.form['port_first']
    port_end = request.form['port_last']
    port_check_result = multi_thread_port_check(ipAdress, port_start, port_end)
    #return 'IP/Domain: {} at {} '.format(ipAdress, port_check_result)
    #flash("Please Wait for the results")
    return render_template('thread-form.html', ipAdress=ipAdress, port_check_result=port_check_result)



if __name__ == '__main__':
    app.run(debug=True)
