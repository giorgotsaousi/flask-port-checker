
import socket
import threading
from queue import Queue
socket.setdefaulttimeout(0.25)

def multi_thread_port_check(ipAdress, port_start, port_end):

    print_lock = threading.Lock()

    target = ipAdress
    target_port_start = int(port_start) 
    target_port_end = int(port_end)
    result_list = []


    t_IP = socket.gethostbyname(target)
    print ('Starting scan on host: ', t_IP)

    def portscan(port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            con = s.connect((t_IP, port))
            with print_lock:
                print(port, 'is open')
                result_list.append(port)
            con.close()
        except:
            pass
        

    def threader():
        while True:
            worker = q.get()
            portscan(worker)
            q.task_done()
            
    q = Queue()
    
    for x in range(200):
        t = threading.Thread(target = threader)
        t.daemon = True
        t.start()
    
    for worker in range(target_port_start, target_port_end):
        q.put(worker)
    
    q.join()
    return result_list

